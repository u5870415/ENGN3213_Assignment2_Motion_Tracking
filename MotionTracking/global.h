﻿/*
 * global.h
 *
 * Created: 2018/4/29 上午 10:11:32
 *  Author: Saki
 */ 

#ifndef _GLOBAL_H_
#define _GLOBAL_H_

#include <stdint.h>

// Type definitions
//  Shorten description
#define uchar       unsigned char
//  Boolean type supports.
#define BOOL        unsigned char
#define TRUE        1
#define FALSE       0

// Functions
//  Transfer a value to the bit value
#define M_BIT(x)            (1<<(x))
#define M_SET_BIT(v, x)     ((v) |= M_BIT(x))
#define M_UNSET_BIT(v, x)   (v &= ~M_BIT(x))
#define M_IS_SET(v, x)      (v & M_BIT(x))

#define NAV_RES_ACCEL_2     0.00006103515625
#define NAV_RES_ACCEL_4     0.000122
#define NAV_RES_ACCEL_8     0.000244
#define NAV_RES_ACCEL_16    0.000732
#define NAV_RES_GYRO_245    0.00875
#define NAV_RES_GYRO_250    0.007633587
#define NAV_RES_GYRO_500    0.0175
#define NAV_RES_GYRO_2000   0.07
#define NAV_RES_MAG_4       0.00014
#define NAV_RES_MAG_8       0.00029
#define NAV_RES_MAG_12      0.00043
#define NAV_RES_MAG_16      0.00058

// Configurations
//#define MODE_ARDUINO
//#define KARMAN_ENABLE
// For 3213 prototype
//  CPU frequency.
//  3213 prototype board, 20MHz
//#define F_CPU 16000000

#endif /* _GLOBAL_H_ */
