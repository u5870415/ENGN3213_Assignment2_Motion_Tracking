﻿/*
 * kalman.h
 *
 * Created: 2018/5/9 PM 11:21:13
 *  Author: Saki
 */ 


#ifndef KALMAN_H_
#define KALMAN_H_

#include <stdint.h>
#include <math.h>

#define AccelSampleCount    8

typedef struct KalmanFilter
{
    float a_x[10], a_y[10],a_z[10] ,g_x[10] ,g_y[10],g_z[10];
    float Px, Rx, Kx, Sx, Vx, Qx;
    float Py, Ry, Ky, Sy, Vy, Qy;
    float Pz, Rz, Kz, Sz, Vz, Qz;
    float aaxs[AccelSampleCount], aays[AccelSampleCount], aazs[AccelSampleCount];
    float aax, aay, aaz;
    float agx, agy, agz;
} KalmanFilter;

void Kalman_init(KalmanFilter *t);

void Kalman_filter(KalmanFilter *t, float accelX, float accelY, float accelZ, float gyroX, float gyroY, float gyroZ);

#endif /* KALMAN_H_ */