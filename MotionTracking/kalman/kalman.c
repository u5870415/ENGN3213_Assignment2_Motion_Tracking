﻿/*
 * kalman.c
 *
 * Created: 2018/5/14 下午 08:54:10
 *  Author: Saki
 */ 

#include "kalman.h"

void Kalman_init(KalmanFilter *t)
{
    for(uint8_t i=0; i<10; ++i)
    {
        t->a_x[i] = 0.0f; t->g_x[i] = 0.0f;
        t->a_y[i] = 0.0f; t->g_y[i] = 0.0f;
        t->a_z[i] = 0.0f; t->g_z[i] = 0.0f;
    }
    for(uint8_t i=0; i<AccelSampleCount; ++i)
    {
        t->aaxs[i]=0.0f;
        t->aays[i]=0.0f;
        t->aazs[i]=0.0f;
    }
    t->Px = 1.0f; t->aax=0.0f; t->agx=0.0f;
    t->Py = 1.0f; t->aay=0.0f; t->agy=0.0f;
    t->Pz = 1.0f; t->aaz=0.0f; t->agz=0.0f;
}

void Kalman_filter(KalmanFilter *t, float accelX, float accelY, float accelZ, float gyroX, float gyroY, float gyroZ)
{
    float aax = atan(accelY / accelZ) * (-180) / M_PI,
    aay = atan(accelX / accelZ) * 180 / M_PI,
    aaz = atan(accelZ / accelY) * 180 / M_PI;
    // Weight filter.
    long aax_sum = 0, aay_sum = 0, aaz_sum = 0;
    // Apply the filter.
    for(uint8_t i=1; i<AccelSampleCount; ++i)
    {
        t->aaxs[i-1] = t->aaxs[i];
        aax_sum += t->aaxs[i] * i;
        t->aays[i-1] = t->aays[i];
        aay_sum += t->aays[i] * i;
        t->aazs[i-1] = t->aazs[i];
        aaz_sum += t->aazs[i] * i;
    }
    
    t->aaxs[AccelSampleCount-1] = aax;
    aax_sum += aax * AccelSampleCount;
    aax = (aax_sum / (11*AccelSampleCount/2.0)) * 9 / 7.0;
    t->aays[AccelSampleCount-1] = aay;
    aay_sum += aay * AccelSampleCount;
    aay = (aay_sum / (11*AccelSampleCount/2.0)) * 9 / 7.0;
    t->aazs[AccelSampleCount-1] = aaz;
    aaz_sum += aaz * AccelSampleCount;
    aaz = (aaz_sum / (11*AccelSampleCount/2.0)) * 9 / 7.0;
    
    // diff
    t->agx += gyroX;
    t->agy += gyroY;
    t->agz += gyroZ;
    
    /* kalman start */
    t->Sx = 0; t->Rx = 0;
    t->Sy = 0; t->Ry = 0;
    t->Sz = 0; t->Rz = 0;
    for(uint8_t i=1; i<10; ++i)
    {
        t->a_x[i-1] = t->a_x[i];
        t->Sx += t->a_x[i];
        t->a_y[i-1] = t->a_y[i];
        t->Sy += t->a_y[i];
        t->a_z[i-1] = t->a_z[i];
        t->Sz += t->a_z[i];
    }
    t->a_x[9] = aax;
    t->Sx += aax;
    t->Sx /= 10;
    t->a_y[9] = aay;
    t->Sy += aay;
    t->Sy /= 10;
    t->a_z[9] = aaz;
    t->Sz += aaz;
    t->Sz /= 10;
    for(int i=0;i<10;i++)
    {
        t->Rx += (t->a_x[i] - t->Sx) * (t->a_x[i] - t->Sx);
        t->Ry += (t->a_y[i] - t->Sy) * (t->a_y[i] - t->Sy);
        t->Rz += (t->a_z[i] - t->Sz) * (t->a_z[i] - t->Sz);
    }
    t->Rx /= 9;
    t->Ry /= 9;
    t->Rz /= 9;
    
    t->Px += 0.0025;
    t->Kx = t->Px / (t->Px + t->Rx);
    t->agx = t->agx + t->Kx * (aax - t->agx);
    t->Px = (1 - t->Kx) * t->Px;

    t->Py += 0.0025;
    t->Ky = t->Py / (t->Py + t->Ry);
    t->agy = t->agy + t->Ky * (aay - t->agy);
    t->Py = (1 - t->Ky) * t->Py;
    
    t->Pz += 0.0025;
    t->Kz = t->Pz / (t->Pz + t->Rz);
    t->agz = t->agz + t->Kz * (aaz - t->agz);
    t->Pz = (1 - t->Kz) * t->Pz;
}