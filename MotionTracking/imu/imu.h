﻿/*
 * IMU
 * Provides the general IMU read/write and configure driver
 */ 


#ifndef _IMU_H_
#define _IMU_H_

#include <stdint.h>
#include <math.h>

#include "kalman.h"

#define PRESSURE_M          0.0289644
#define PRESSURE_G          9.80665
#define PRESSURE_GBYM       0.28404373326
#define PRESSURE_R          8.31432
#define PRESSURE_SEALEVEL   101325.0

#define NAV_ADDR_AG         0x6B    // LSM9DS1 Accel/Gyro
#define NAV_ADDR_MAG        0x1E    // LSM9DS1 Magnetometer
#define NAV_ADDR_BARO       0x5D    // LPS25HB Barometer
#define NAV_EN_AG           DDD3
#define NAV_EN_MAG          DDD4
#define NAV_EN_BARO         DDD5

#define NAV_DECLINATION     -70.0f
#define NAV_CORSAMPLES      200
#define NAV_ACCEL_STALIMIT  0.006
#define NAV_ACCEL_FUSERATE  0.1
#define NAV_SAMPLE_PERIOD   0.10f
#define NAV_SAMPLE_RATE     (1000.0 / NAV_SAMPLE_PERIOD)

// LPS25HB Register
#define NAV_BR_RES_CONF     0x10
#define NAV_BR_CTRL_REG1    0x20
#define NAV_BR_PRESS_OUTXL  0x28
#define NAV_BR_PRESS_OUTL   0x29
#define NAV_BR_PRESS_OUTH   0x2A
#define NAV_BR_TEMP_OUTL    0x2B
#define NAV_BR_TEMP_OUTH    0x2C

// LSM9DS1 Register
#define NAV_CTRL_REG1_G     0x10
#define NAV_CTRL_REG3_G     0x12
#define NAV_REG_STATUS      0x17
#define NAV_OUT_X_L_G       0x18
#define NAV_CTRL_REG6_XL    0x20
#define NAV_OUT_X_XL        0x28

// LPS25HB Register
#define NAV_CTRL_REG1_M     0x20
#define NAV_CTRL_REG2_M     0x21
#define NAV_CTRL_REG3_M     0x22
#define NAV_CTRL_REG4_M     0x23
#define NAV_REG_STATUS_M    0x27
#define NAV_OUT_X_L_M       0x28

#define NAV_BR_RES_AT1      3
#define NAV_BR_RES_AT0      2
#define NAV_BR_RES_AP1      1
#define NAV_BR_RES_AP0      0

#define NAV_BR_CT1_PD       7
#define NAV_BR_CT1_ODR2     6
#define NAV_BR_CT1_ODR1     5
#define NAV_BR_CT1_ODR0     4
#define NAV_BR_CT1_DFFEN    3
#define NAV_BR_CT1_BDU      2

#define NAV_CTRREG1_FSG0    3
#define NAV_CTRREG1_FSG1    4
#define NAV_CTRREG1_ODR0    5
#define NAV_CTRREG1_ODR1    6
#define NAV_CTRREG1_ODR2    7

#define NAV_CTRREG3_HPF0    0
#define NAV_CTRREG3_HPF1    1
#define NAV_CTRREG3_HPF2    2
#define NAV_CTRREG3_HPF3    3
#define NAV_CTRREG3_HPEN    6

#define NAV_CTRREG6_FS0     3
#define NAV_CTRREG6_FS1     4
#define NAV_CTRREG6_ODR0    5
#define NAV_CTRREG6_ODR1    6
#define NAV_CTRREG6_ODR2    7

#define NAV_CTRREG1_M_DO0   2
#define NAV_CTRREG1_M_DO1   3
#define NAV_CTRREG1_M_DO2   4
#define NAV_CTRREG1_M_OM0   5
#define NAV_CTRREG1_M_OM1   6
#define NAV_CTRREG1_M_TMP   7

#define NAV_CTRREG2_M_FS0   5
#define NAV_CTRREG2_M_FS1   6

#define NAV_CTRREG3_M_MD0   0
#define NAV_CTRREG3_M_MD1   1

#define NAV_CTRREG4_M_OMZ0  2
#define NAV_CTRREG4_M_OMZ1  3

typedef struct NavStateData
{
    uint8_t cache[6];
    int16_t axisX, axisY, axisZ, axisX2, axisY2, axisZ2;
    uint32_t cache32;
    int32_t gxo, gyo, gzo, mzo;
    float gyroX, gyroY, gyroZ;
    float accelX, accelY, accelZ;
    float magX, magY, magZ;
    float magHeading, initHeading;
    float temperature;
    float pressure;
} NavStateData;

static NavStateData navStatus;
static KalmanFilter navKalman;

typedef struct NavPosture
{
    float roll, pitch, heading;
    float altitude;
} NavPosture;

static NavPosture navPosture;

static uint8_t sensorStatus = 0x00;

#define Nav_agRead(subAddress, data) \
    Twi_read(NAV_ADDR_AG, subAddress, &data, 1)

#define Nav_agReadBytes(subAddress, data, length) \
    Twi_read(NAV_ADDR_AG, subAddress, data, length)

#define Nav_agWrite(subAddress, data) \
    Twi_write(NAV_ADDR_AG, subAddress, data)

#define Nav_mRead(subAddress, data) \
    Twi_read(NAV_ADDR_MAG, subAddress, &data, 1)

#define Nav_mReadBytes(subAddress, data, length) \
    Twi_read(NAV_ADDR_MAG, subAddress, data, length)

#define Nav_mWrite(subAddress, data) \
    Twi_write(NAV_ADDR_MAG, subAddress, data)

#define Nav_bRead(subAddress, data) \
    Twi_read(NAV_ADDR_BARO, subAddress, &data, 1);

#define Nav_bReadBytes(subAddress, data, length) \
    Twi_read(NAV_ADDR_BARO, subAddress, data, length)

#define Nav_bWrite(subAddress, data) \
    Twi_write(NAV_ADDR_BARO, subAddress, data)
    
// Configure the LSM9DS1
// Gyroscope running mode:
//   500 dps, 238 Hz ODR, 76 Hz Cut-off
//   High-pass filter enabled, Cut-off freq: 4 Hz
// Accelerometer running mode:
//   238 Hz ODR, +/- 4g
// Magnetometer running mode:
//   80 Hz ODR, +/- 4 gauss, continuous mode
// Barometer running mode:
//   12.5 Hz ODR, Resolution 32 Temperature, 128 Pressure
#define Nav_init() \
    Nav_agWrite(NAV_CTRL_REG1_G, M_BIT(NAV_CTRREG1_ODR2) | M_BIT(NAV_CTRREG1_FSG0)); \
    Nav_agWrite(NAV_CTRL_REG3_G, M_BIT(NAV_CTRREG3_HPEN) | M_BIT(NAV_CTRREG3_HPF1)); \
     \
    Nav_agWrite(NAV_CTRL_REG6_XL, M_BIT(NAV_CTRREG6_ODR2) | M_BIT(NAV_CTRREG6_FS1)); \
     \
    Nav_mWrite(NAV_CTRL_REG1_M, M_BIT(NAV_CTRREG1_M_OM1) | M_BIT(NAV_CTRREG1_M_OM0) | M_BIT(NAV_CTRREG1_M_DO2) | M_BIT(NAV_CTRREG1_M_DO1) | M_BIT(NAV_CTRREG1_M_DO0)); \
    Nav_mWrite(NAV_CTRL_REG2_M, M_BIT(NAV_CTRREG2_M_FS0) | M_BIT(NAV_CTRREG2_M_FS1)); \
    Nav_mWrite(NAV_CTRL_REG3_M, 0x00); \
    Nav_mWrite(NAV_CTRL_REG4_M, M_BIT(NAV_CTRREG4_M_OMZ1) | M_BIT(NAV_CTRREG4_M_OMZ0)); \
     \
    Nav_bWrite(NAV_BR_RES_CONF, M_BIT(NAV_BR_RES_AT1) | M_BIT(NAV_BR_RES_AP1)); \
    Nav_bWrite(NAV_BR_CTRL_REG1, M_BIT(NAV_BR_CT1_PD) | M_BIT(NAV_BR_CT1_ODR2) | M_BIT(NAV_BR_CT1_DFFEN) | M_BIT(NAV_BR_CT1_BDU));

#define Nav_updateBaro() \
    Nav_bRead(NAV_BR_TEMP_OUTH, navStatus.cache[0]); \
    Nav_bRead(NAV_BR_TEMP_OUTL, navStatus.cache[1]); \
    navStatus.axisX = ((uint16_t)navStatus.cache[0] << 8) | navStatus.cache[1]; \
    navStatus.temperature = (float)(navStatus.axisX) / 480.0f + 42.5f; \
    Nav_bRead(NAV_BR_PRESS_OUTH, navStatus.cache[0]); \
    Nav_bRead(NAV_BR_PRESS_OUTL, navStatus.cache[1]); \
    Nav_bRead(NAV_BR_PRESS_OUTXL, navStatus.cache[2]); \
    navStatus.cache32 = ( ((uint32_t)navStatus.cache[0] << 24) | ((uint32_t)navStatus.cache[1] << 16) | ((uint32_t)navStatus.cache[2] << 8)) >> 8; \
    navStatus.pressure = (float)navStatus.cache32 / 4096.0f;

#define Nav_updateInts() \
    navStatus.axisX = ((int16_t)navStatus.cache[1] << 8) | navStatus.cache[0]; \
    navStatus.axisY = ((int16_t)navStatus.cache[3] << 8) | navStatus.cache[2]; \
    navStatus.axisZ = ((int16_t)navStatus.cache[5] << 8) | navStatus.cache[4];

#define Nav_updateInts2() \
    navStatus.axisX2 = ((int16_t)navStatus.cache[1] << 8) | navStatus.cache[0]; \
    navStatus.axisY2 = ((int16_t)navStatus.cache[3] << 8) | navStatus.cache[2]; \
    navStatus.axisZ2 = ((int16_t)navStatus.cache[5] << 8) | navStatus.cache[4];

#define Nav_updateAgValue(regaddr) \
    Nav_agReadBytes(regaddr, navStatus.cache, 6); \
    Nav_updateInts();
  
#define Nav_scaleGryo(scaler, DT) \
    navStatus.gyroX = (float) - (navStatus.axisX - navStatus.gxo) * scaler * DT; \
    navStatus.gyroY = (float) - (navStatus.axisY - navStatus.gyo) * scaler * DT; \
    navStatus.gyroZ = (float) - (navStatus.axisZ - navStatus.gzo) * scaler * DT;

#define Nav_scaleAccel(scaler) \
    navStatus.accelX = (float) navStatus.axisX2 * scaler; \
    navStatus.accelY = (float) navStatus.axisY2 * scaler; \
    navStatus.accelZ = (float) navStatus.axisZ2 * scaler;

#define Nav_updateMValue(regaddr) \
    Nav_mReadBytes(regaddr, navStatus.cache, 6); \
    Nav_updateInts();

#define Nav_scaleMag(scaler) \
    navStatus.magX = (float) navStatus.axisX * scaler - 0.0617f; \
    navStatus.magY = (float) navStatus.axisY * scaler - 0.15885f; \
    navStatus.magZ = (float) (navStatus.axisZ - navStatus.mzo) * scaler + 0.21000f;
    
#define Nav_updateAccel(regaddr, scaler) \
    Nav_updateAgValue(regaddr)

#define Nav_updateMag(regaddr) \
    Nav_updateMValue(regaddr);

#define Nav_readGyroAndAccel() \
    Nav_agRead(NAV_REG_STATUS, sensorStatus); \
    if((sensorStatus & 0x07) == 0x07) \
    { \
        Nav_agReadBytes(NAV_OUT_X_L_G, navStatus.cache, 6); \
        Nav_updateInts(); \
        Nav_agReadBytes(NAV_OUT_X_XL, navStatus.cache, 6); \
        Nav_updateInts2(); \
    }

#define Nav_scaleGyroAndAccel() \
    Nav_scaleGryo(NAV_RES_GYRO_500, NAV_SAMPLE_PERIOD); \
    Nav_scaleAccel(NAV_RES_ACCEL_4);

#define Nav_readMag() \
    Nav_mRead(NAV_REG_STATUS_M, sensorStatus); \
    if((sensorStatus & 0x07) == 0x07) \
    { \
        Nav_updateMag(NAV_OUT_X_L_M); \
    }

static inline void Nav_fusePressure()
{
    float c = navStatus.pressure * 100.0f,
          t = navStatus.temperature + 273.15f,
          nAltitude;
    float dByC = PRESSURE_SEALEVEL / c,
          cByD = c / PRESSURE_SEALEVEL;
    if (dByC >= 18.5072211f) {
        // Out of range.
        navPosture.altitude = 20000.0f;
        return;
    }
    // Calculate the pressure.
    if (dByC < 4.4770480f)
    {
        nAltitude = ((t * ((1 / pow(cByD, (PRESSURE_R * -0.0065) / PRESSURE_GBYM)) - 1)) / -0.0065);
        navPosture.altitude = navPosture.altitude * 0.98f + nAltitude * 0.02f;
        return;
    }
    if(dByC < (101325 / 5474.89))
    {
        nAltitude = ((PRESSURE_R * (t - 71.5) * (cByD+0.65099175902911916462842137467371)) / (-PRESSURE_GBYM)) + 11000;
        navPosture.altitude = navPosture.altitude * 0.98f + nAltitude * 0.02f;
    }
}

#define NAV_CALIBRATE_TIMES     20
#define NAV_SAMPLE_TIMES        5
#define NAV_STATIONARY_LIMIT    70

#define Nav_calibrateUpdateAccel(x) \
    diffAccelX[x] = abs(navStatus.axisX2 - lastAccelX); \
    diffAccelY[x] = abs(navStatus.axisY2 - lastAccelY); \
    diffAccelZ[x] = abs(navStatus.axisZ2 - lastAccelZ); \
    lastAccelX = navStatus.axisX2; \
    lastAccelY = navStatus.axisY2; \
    lastAccelZ = navStatus.axisZ2;

#define NAV_MAG_FUSE_RATE       0.80f

void Nav_fuseMag(float mx, float my, float mz)
{
    // Calculate the heading
    navStatus.magHeading = atan2(my, -mx);
    float nHeading = 360.0f - (navStatus.magHeading * 180.0 / M_PI);
    if(navPosture.heading < 100.0f && nHeading > 200.0f)
    {
        navPosture.heading = (navPosture.heading + 360.0f) * (1.0f-NAV_MAG_FUSE_RATE) + nHeading * NAV_MAG_FUSE_RATE;
    }
    else if(nHeading < 100.0f && navPosture.heading > 200.0f)
    {
        navPosture.heading = (navPosture.heading - 360.0f) * (1.0f-NAV_MAG_FUSE_RATE) + nHeading * NAV_MAG_FUSE_RATE;
    }
    else
    {
        navPosture.heading = navPosture.heading * (1.0f-NAV_MAG_FUSE_RATE) + nHeading * NAV_MAG_FUSE_RATE;
    }
    
    if(navPosture.heading < 0.0f)
    {
        navPosture.heading += 360.0f;
    }
    else if(navPosture.heading > 360.0f)
    {
        navPosture.heading -= 360.0f;
    }
}

void Nav_calibrate()
{
    int8_t i;
    uint8_t calibrateTime = NAV_CALIBRATE_TIMES;
    int16_t diffAccelX[NAV_SAMPLE_TIMES], diffAccelY[NAV_SAMPLE_TIMES], diffAccelZ[NAV_SAMPLE_TIMES],
            gyroX, gyroY, gyroZ, magZ,
            lastAccelX = 0, lastAccelY = 0, lastAccelZ = 0;
    for(i=NAV_SAMPLE_TIMES-1; i>=0; --i)
    {
        Nav_readGyroAndAccel();
        // Update accelerometer data.
        Nav_calibrateUpdateAccel(i);
    }
    // Reset the value.
    navStatus.gxo = 0; navStatus.gyo = 0; navStatus.gzo = 0;
    BOOL isStationary;
    // Keep tracking until find the stationary states.
    while(calibrateTime > 0)
    {
        // Read the barometer.
        Nav_updateBaro();
        // Shift the cache.
        isStationary = TRUE;
        for(i=1; i<NAV_SAMPLE_TIMES; ++i)
        {
            diffAccelX[i] = diffAccelX[i-1];diffAccelY[i] = diffAccelY[i-1];diffAccelZ[i] = diffAccelZ[i-1];
            isStationary = isStationary && (diffAccelX[i] < NAV_STATIONARY_LIMIT)
                                        && (diffAccelY[i] < NAV_STATIONARY_LIMIT)
                                        && (diffAccelZ[i] < NAV_STATIONARY_LIMIT);
        }
        // Update the latest data.
        Nav_readGyroAndAccel();
        // Update the accel data.
        Nav_calibrateUpdateAccel(0);
        isStationary = isStationary && (diffAccelX[0] < NAV_STATIONARY_LIMIT)
                                    && (diffAccelY[0] < NAV_STATIONARY_LIMIT)
                                    && (diffAccelZ[0] < NAV_STATIONARY_LIMIT);
        gyroX = navStatus.axisX;
        gyroY = navStatus.axisY;
        gyroZ = navStatus.axisZ;
        // Read the magnetometer.
        Nav_readMag();
        magZ = navStatus.axisZ;
        if(isStationary)
        {
            --calibrateTime;
            // Gyro correction start.
            navStatus.gxo += gyroX; navStatus.gyo += gyroY; navStatus.gzo += gyroZ;
            navStatus.mzo += magZ;
        }
        else
        {
            // Reset the value.
            calibrateTime = NAV_CALIBRATE_TIMES;
            navStatus.gxo = 0; navStatus.gyo = 0; navStatus.gzo = 0;
            navStatus.mzo = 0;
        }
    }
    navStatus.gxo /= NAV_CALIBRATE_TIMES; navStatus.gyo /= NAV_CALIBRATE_TIMES; navStatus.gzo /= NAV_CALIBRATE_TIMES;
    navStatus.mzo /= NAV_CALIBRATE_TIMES;
}

static inline void Nav_kalman()
{
    // Read the data.
    Nav_readGyroAndAccel();
    // Scale the data.
    Nav_scaleGyroAndAccel();
    // Kalman filter.
    Kalman_filter(&navKalman, navStatus.accelX, navStatus.accelY, navStatus.accelZ, navStatus.gyroX, navStatus.gyroY, navStatus.gyroZ);
    // Update the posture.
    navPosture.roll = navKalman.agx;
    navPosture.pitch = navKalman.agy;
}

#endif /* _TWI_H_ */
