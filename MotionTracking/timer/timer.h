﻿/*
 * timer.h
 *
 * Created: 2018/5/9 PM 09:46:00
 *  Author: Saki
 */ 

#ifndef TIMER_H_
#define TIMER_H_

#include <avr/io.h>

#define OCR1_VALUE_1HZ      (19531>>1)
#define OCR1_VALUE_10HZ     (1953>>1)
#define OCR1_VALUE_20HZ     (976>>1)
#define OCR1_VALUE_100HZ    (195>>1)
#define OCR1_VALUE_1000HZ   (19>>1)

#define Timer1A_init() \
    TCCR1A |= M_BIT(COM1A0); \
    TCCR1B |= M_BIT(WGM12) | M_BIT(CS12) | M_BIT(CS10); \
    TIMSK1 |= M_BIT(OCIE0A);

#define Timer1A_setInterval(x) \
    OCR1AH = (uint8_t)((x) >> 8); \
    OCR1AL = (uint8_t)(x);

#endif /* TIMER_H_ */