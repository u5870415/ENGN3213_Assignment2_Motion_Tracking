
#define F_CPU 20000000UL

#include "global.h"

#include <util/delay.h>
#include <util/atomic.h>

#include <stdlib.h>
#include <stdio.h>

#include <avr/io.h>
#include <avr/interrupt.h>

#include "usart.h"
#include "twi.h"
#include "imu.h"
#include "timer.h"
#include "kalman.h"
#include "mpu6050.h"

#define OUTPUT_COUNTER_LIMIT    3
#define OUTPUT_KALMAN_LIMIT     7

uint8_t outputCounter = 0x00, kalmanCounter = 0x00;
char str[50], surfaceStr[50];

float am_angle_mat[9]={0,0,0,0,0,0,0,0,0};
float gyro_angle_mat[9]={0,0,0,0,0,0,0,0,0};

#define TIMER_STATE_1A  0
static uint8_t timerStates = 0x00;

#define Timer1A_isTrigger() \
    (M_IS_SET(timerStates, TIMER_STATE_1A))

#define Timer1A_trigger() \
    M_SET_BIT(timerStates, TIMER_STATE_1A)

ISR(TIMER1_COMPA_vect)
{
    // Set the trigger flag.
    timerStates = 0x01;
}

void setup()
{
    DDRB = M_BIT(DDB1) | M_BIT(DDB2);
    // Enable PmodNAV all modules.
    DDRD |= M_BIT(NAV_EN_AG) | M_BIT(NAV_EN_MAG) | M_BIT(NAV_EN_BARO);
    PORTD |= M_BIT(NAV_EN_AG) | M_BIT(NAV_EN_MAG) | M_BIT(NAV_EN_BARO);
    // Initialized communication ports.
    Usart_init();
    Twi_init(DDC5, DDC4);
    // Initialized timer.
    Timer1A_init();
    Timer1A_setInterval(OCR1_VALUE_20HZ);
    // Initialized IMUs.
    Nav_init();
    Mpu_Init();
    
    // Enable interrupts.
    sei();

    // Detect the stationary state.
    M_UNSET_BIT(PORTB, DDB2);
    Nav_calibrate();
    Mpu_calibrate();
    M_SET_BIT(PORTB, DDB2);
}

float maxAxis = 0.0f, minAxis = 0.0f;
float dt = 0.001f;

void loop()
{
    // Check the timer.
    if (Timer1A_isTrigger())
    {
        // Kalman filter the Gryo and Accel data.
        Mpu_kalman();
        Nav_kalman();
        // Read the barometer.
        Nav_updateBaro();
        // Read the magnetometer.
        Nav_readMag();
        Nav_scaleMag(NAV_RES_MAG_16);
        Nav_fuseMag(navStatus.magX, navStatus.magY, navStatus.magZ);
        
        /*// Read the gyroscope and accelerometer status.
        Nav_readGyroAndAccel();
        
        
        
        // Fuse the accelerometer and magnetometer data.
        Nav_fuseAccelMag(navStatus.accelX, navStatus.accelY, navStatus.accelZ, navStatus.magX, navStatus.magY, navStatus.magZ);
        // Fuse the gyroscope data.
        Nav_fuseGyro(navStatus.gyroX, navStatus.gyroY, navStatus.gyroZ);
        */
        // Fuse the barometer data.
        Nav_fusePressure();
        
        if(outputCounter == OUTPUT_COUNTER_LIMIT)
        {
            ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
                dtostrf(mpuKalman.agy, 7, 3, str);
                dtostrf(mpuKalman.agx, 7, 3, str+8);
                dtostrf(navPosture.heading, 7, 3, str+16);
                dtostrf(navPosture.altitude, 7, 3, str+24);
                dtostrf(navStatus.temperature, 7, 3, str+32);
                str[7] = ',';str[15] = ',';str[23] = ',';       str[31] = ',';
                str[39] = '\r';
                str[40] = '\n';
            }
            Usart_writeStr(str);
            outputCounter = 0;
        }
        // Increase the counter.
        ++outputCounter;
        // Clear the flag.
        timerStates = 0x00;
    }
    // Just delay a little bit.
    _delay_us(1);
}

int main(void)
{
    setup();
    while(1)
    {
        loop();
    }
}
