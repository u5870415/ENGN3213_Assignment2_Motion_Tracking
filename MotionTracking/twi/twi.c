﻿
#include <util/delay.h>
#include <util/twi.h>

#include "twi.h"

#define Twi_waitTrans(result) \
  while(!Twi_isSent()) ; \
  if(!Twi_isStatus(result)) \
  { \
    return 0; \
  }

#define Twi_transfer(data, result) \
  Twi_setData(data); \
  Twi_receive(result);

#define Twi_receive(result) \
  Twi_startTrans(); \
  Twi_waitTrans(result);

#define Twi_receiveAck(result) \
  Twi_startTransAck(); \
  Twi_waitTrans(result);

uint8_t Twi_read(uint8_t address, uint8_t subAddress, uchar *data, uint8_t length)
{
  // 1. Start.
  Twi_sendStart();
  Twi_waitTrans(TW_START);

  // 2. Send SLA+W (Write Mode)
  Twi_transfer((address << 1) | TW_WRITE, TW_MT_SLA_ACK);

  // 3. Send Data #1 (sub-address)
  if (length > 1)
  {
    subAddress |= 0x80; // Auto Increment? No reference here.
  }
  Twi_transfer(subAddress, TW_MT_DATA_ACK);

  // 4. We need to change to Read mode so Re-start (repeated)
  Twi_sendStart();
  Twi_waitTrans(TW_REP_START);

  // 5. Send SLA+R (Read mode)
  Twi_transfer((address << 1) | TW_READ, TW_MR_SLA_ACK);
  // Read the other datas.
  if (length > 1)
  {
    for (uint8_t i=0; i<length-1; ++i)
    {
      // 6. Data up to #(N-1) (Read a byte). Detect ACK.
      Twi_receiveAck(TW_MR_DATA_ACK);
      // Read the data from TWDR.
      *data++ = TWDR;
    }
  }

  // 6. Data #2 or #N (Read a byte). Last byte needs a NACK
  Twi_receive(TW_MR_DATA_NACK);
  *data = TWDR; // Last byte

  // 7. Stop.
  Twi_sendStop();
  _delay_ms(1);
  
  return length;
}

uint8_t Twi_write(uint8_t address, uint8_t subAddress, uint8_t data)
{
  // 1. Start
  Twi_sendStartAck();
  Twi_waitTrans(TW_START);

  // 2. Send SLA+W (Write Mode)
  Twi_transfer((address << 1) | (TW_WRITE), TW_MT_SLA_ACK);

  // 3. Send Data #1 (sub-address)
  Twi_transfer(subAddress, TW_MT_DATA_ACK);

  // 4. Send Data #2 (actual data)
  Twi_transfer(data, TW_MT_DATA_ACK);

  // 5. Stop condition
  Twi_sendStop();
  _delay_ms(1);

  return 1;
}

