﻿/*
 * twi.h
 * Provides the general I2C master driver
 */ 


#ifndef _TWI_H_
#define _TWI_H_

#include "global.h"

#include <avr/io.h>

// Initialize the I2C with fast mode
#define Twi_init(pin1, pin2) \
  PORTC = M_BIT(pin1) | M_BIT(pin2); \
  TWCR = M_BIT(TWEN); \
  TWDR = 0xFF; \
  Twi_setFreq(400000)

#define Twi_setFreq(x) \
  TWBR = ((F_CPU / (x)) - 16) >> 1

#define Twi_sendStart() \
  TWCR = M_BIT(TWINT) | M_BIT(TWEN) | M_BIT(TWSTA)

#define Twi_sendStartAck() \
  TWCR = M_BIT(TWINT) | M_BIT(TWEN) | M_BIT(TWSTA) | M_BIT(TWEA);

#define Twi_sendStop() \
  TWCR = M_BIT(TWINT) | M_BIT(TWEN)| M_BIT(TWSTO)

#define Twi_startTrans() \
  TWCR = M_BIT(TWINT) | M_BIT(TWEN)

#define Twi_startTransAck() \
  TWCR = M_BIT(TWINT) | M_BIT(TWEN) | M_BIT(TWEA)

#define Twi_setData(x) \
  TWDR = (x)

#define Twi_isSent() \
  (M_IS_SET(TWCR, TWINT))

#define Twi_isStatus(x) \
  ((TWSR & 0xF8) == (x))

uint8_t Twi_read(uint8_t address, uint8_t subAddress, uchar *data, uint8_t length);

uint8_t Twi_write(uint8_t address, uint8_t subAddress, uint8_t data);

#endif /* _TWI_H_ */
